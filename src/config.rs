use std::fs::File;
use std::io::{ BufReader, BufRead, Error };

pub struct Config
{
  channel_disable: bool,
  host_lookups: bool,
  show_ips: bool,
  no_dup_ips: bool,
  force_password: bool,
  force_names: bool,
  no_dup_names: bool,
  no_name_changing: bool,
  kick_dups: bool,
  name_register: bool,
  logfile_on: bool,
  run_as_daemon: bool,
  debug: bool,

  max_idle_time: u32,
  censor_level: u8,
  register_level: i8,
  login_speed: u32,
  port: u32,
  ssl_port: u32,

  bad_words: Vec<String>,
  //no_owner: Vec<String>,
  sysop_password: String,
  pub welcome_message: String,
  welcome_message_set: bool,

  passwd: String,
  logfile: String,
  ssl_cert: String,
  run_as: String,

  //allowed_hosts: Vec<String>,
  banned_hosts: Vec<String>,
}

impl Config
{
  pub fn init() -> Config
  {
    Config
    {
      channel_disable: false,
      host_lookups: true,
      show_ips: true,
      no_dup_ips: false,
      force_password: false,
      force_names: false,
      no_dup_names: false,
      no_name_changing: false,
      kick_dups: false,
      name_register: false,
      logfile_on: false,
      run_as_daemon: true,
      debug: false,

      max_idle_time: 60,
      censor_level: 0,
      register_level: -1,
      login_speed: 3,
      port: 6666,
      ssl_port: 6667,

      bad_words: Vec::new(),
      //no_owner: Vec::new(),
      sysop_password: String::from("pencil"),
      welcome_message: String::from("Naken Chat\n"),
      welcome_message_set: false,

      passwd: String::from(""),
      logfile: String::from(""),
      ssl_cert: String::from(""),
      run_as: String::from(""),

      //allowed_hosts: Vec::new(),
      banned_hosts: Vec::new(),
    }
  }

  pub fn read_config(&mut self, filename: &str) -> Result<(), Error>
  {
    let file = File::open(filename) ?;
    let buf_reader = BufReader::new(file);

    for line in buf_reader.lines()
    {
      //println!("{}", line ?);
      let option = line.unwrap();

      if option.len() == 0 { continue }


      let index = option.find(" ");
      if index == None { continue; }

      let (key, value_str) = option.split_at(index.unwrap());
      let value_raw = value_str.to_string();
      let value = value_raw.trim().to_string();

      match key
      {
        "CHANNEL_DISABLE" => self.channel_disable = Config::get_boolean(value),
        "HOST_LOOKUPS" => self.host_lookups = Config::get_boolean(value),
        "SHOW_IPS" => self.show_ips = Config::get_boolean(value),
        "FORCE_PASSWORD" => self.force_password = Config::get_boolean(value),
        "NO_DUP_NAMES" => self.no_dup_names = Config::get_boolean(value),
        "NO_NAME_CHANGING" => self.no_name_changing = Config::get_boolean(value),
        "KICK_DUPS" => self.kick_dups = Config::get_boolean(value),
        "NAME_REGISTER" => self.name_register = Config::get_boolean(value),
        "LOGFILE_ON" => self.logfile_on = Config::get_boolean(value),
        "RUN_AS_DAEMON" => self.run_as_daemon = Config::get_boolean(value),
        "DEBUG" => self.debug = Config::get_boolean(value),
        "MAX_IDLE_TIME" => self.max_idle_time = value.parse::<u32>().unwrap(),
        "CENSOR_LEVEL" => self.censor_level = value.parse::<u8>().unwrap(),
        "REGISTER_LEVEL" => self.register_level = value.parse::<i8>().unwrap(),
        "LOGIN_SPEED" => self.login_speed = value.parse::<u32>().unwrap(),
        "PORT" => self.port = value.parse::<u32>().unwrap(),
        "SSL_PORT" => self.ssl_port = value.parse::<u32>().unwrap(),
        "BAD_WORD" => self.bad_words.push(value),
        "SYSOP_PASSWORD" => self.sysop_password = value,
        "WELCOME" => {
          if self.welcome_message_set == false
          {
            self.welcome_message = "".to_string();
            self.welcome_message_set = true;
          }
          self.welcome_message.push_str(&value);
          self.welcome_message.push_str("\n");
        },
        "PASSWD" => self.passwd = value,
        "LOGFILE" => self.logfile = value,
        "SSL_CERT" => self.ssl_cert = value,
        "RUN_AS" => self.run_as = value,
        "BAN" => self.banned_hosts.push(value),
        _ => ()
      }
    }

    Ok(())
  }

  fn get_boolean(value: String) -> bool
  {
    if value == "true" || value == "True" { return true; }
    if value == "1" { return true; }

    return false;
  }

  pub fn dump(&self)
  {
    println!(" channel_disable: {}", self.channel_disable);
    println!("    host_lookups: {}", self.host_lookups);
    println!("        show_ips: {}", self.show_ips);
    println!("      no_dup_ips: {}", self.no_dup_ips);
    println!("  force_password: {}", self.force_password);
    println!("     force_names: {}", self.force_names);
    println!("    no_dup_names: {}", self.no_dup_names);
    println!("no_name_changing: {}", self.no_name_changing);
    println!("       kick_dups: {}", self.kick_dups);
    println!("   name_register: {}", self.name_register);
    println!("      logfile_on: {}", self.logfile_on);
    println!("   run_as_daemon: {}", self.run_as_daemon);
    println!("           debug: {}", self.debug);

    println!("   max_idle_time: {}", self.max_idle_time);
    println!("    censor_level: {}", self.censor_level);
    println!("  register_level: {}", self.register_level);
    println!("     login_speed: {}", self.login_speed);
    println!("            port: {}", self.port);
    println!("        ssl_port: {}", self.ssl_port);

    println!("       bad_words: {}", self.bad_words.len());
    println!("  sysop_password: {}", self.sysop_password);
    println!(" welcome_message: {}", self.welcome_message);
    println!("          passwd: {}", self.passwd);
    println!("         logfile: {}", self.logfile);
    println!("        ssl_cert: {}", self.ssl_cert);
    println!("          run_as: {}", self.run_as);
    //println!("   allowed_hosts: {}", self.allowed_hosts.len());
    println!("    banned_hosts: {}", self.banned_hosts.len());
  }
}

