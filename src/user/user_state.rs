use std::fmt;

pub enum UserState
{
  Free,
  Connecting,
  Connected,
}

impl fmt::Display for UserState
{
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
  {
    match *self
    {
      UserState::Free => write!(f, "Free"),
      UserState::Connecting => write!(f, "Connecting"),
      UserState::Connected => write!(f, "Connected"),
    }
  }
}

