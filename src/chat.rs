use tokio::net::{ TcpStream };
use tokio::io::{ AsyncBufReadExt, AsyncWriteExt };
use std::sync::{ Arc };
//use tokio::runtime::Builder;

//use config::Config;

pub struct Chat
{
}

impl Chat
{
  //pub async fn handle_client(
  pub async fn handle_client(
    mut stream: &TcpStream,
    user_id: i32,
    users: Arc<Vec<crate::user::User<'_>>>,
    config: Arc<crate::config::Config>)
  {
    let (reader, mut writer) = stream.split();
    let user_id: usize = user_id as usize;

    //let a: i32 = writer;
    //let a: tokio::net::tcp::WriteHalf = writer;

    users[user_id].writer = Some(& mut writer);
    //users[user_id].writer = Some(& mut tokio::io::BufWriter::new(writer));

    let mut reader = tokio::io::BufReader::new(reader);
    let mut line = String::new();

    //users[user_id].writer.write_all(config.welcome_message.as_bytes()).await.unwrap();

    match users[user_id].writer
    {
      Some(writer) => writer.write_all(config.welcome_message.as_bytes()).await.unwrap(),
    }
    //writer.flush();

    while reader.read_line(&mut line).await.unwrap() != 0
    {
      line.clear();
    }

    println!("done");

    stream.shutdown();
  }

/*
  async fn send_to_all(
    String &line
  )
  {
    let text = format!("[{}]{}: {}", user_id, "mike", line);
    // Use push_str() or concat!()
    writer.write_all(text.as_bytes()).await.unwrap();
  }
*/
}

