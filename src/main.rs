//use std::thread;
//use std::time::SystemTime;
//use std::net::{ TcpListener, TcpStream, Shutdown };
//use tokio::net::{ TcpListener, TcpStream };
use tokio::net::{ TcpListener };
//use tokio::io::{ AsyncBufReadExt, AsyncWriteExt };
use tokio::runtime::Builder;
//use std::io::{ Read, Write };
//use mdns::{ Error };
use std::sync::Arc;

pub mod chat;
pub mod config;
pub mod user;

const MAX_USERS: usize = 20;

async fn accept_connections<'a>(
  config: config::Config) -> std::io::Result<()>
{
  let mut users: Vec<user::User> = Vec::new();

  users.reserve_exact(MAX_USERS);

  for _n in 0..MAX_USERS
  {
    users.push(user::User::init());
  }

  // Accept connections and process them.
  let listener = TcpListener::bind("0.0.0.0:6666").await.unwrap();
  let users = Arc::new(users);
  let config = Arc::new(config);

  //let users = &users;

  //let users = (&'a std::sync::Arc<&Vec<crate::user::User>>&users;
  //let users = users as &'a std::sync::Arc<&Vec<crate::user::User>>;

  println!("Server listening on port 6666");

  loop
  {
    let (stream, _) = listener.accept().await?;

    let mut user_id: i32 = -1;

println!("Accepted socket");

    for n in 0..MAX_USERS
    {
      if users[n].is_free()
      {
        user_id = n as i32;
        break;
      }
    }

println!("user_id={}", user_id);

    let i: usize = user_id as usize;

    tokio::spawn(chat::Chat::handle_client(
       &stream,
       user_id,
       users.clone(),
       config.clone()));
  }
}

//#[tokio::main]
fn main()
{
  let mut config = config::Config::init();

  //let users = users as &'a std::sync::Arc<&Vec<crate::user::User>>;
  //let users: std::sync::Arc<&Vec<crate::user::User>> = &users;

  config.read_config("naken_chat.conf").expect("Couldn't read config.");
  config.dump();

  //users[0].dump();

  //let mut basic_rt = runtime::Builder::new()
  //  .basic_scheduler()
  //  .build().unwrap();

  //let mut basic_rt = runtime::Builder.new_current_thread();
  let basic_rt = Builder::new_current_thread().enable_io().build().unwrap();

  //loop
  {
    //tokio::spawn(accept_connections());
    //basic_rt.block_on(accept_connections());
    //basic_rt.spawn(accept_connections());
    basic_rt.block_on(accept_connections(config));
  }

  //basic_rt.shutdown_on_idle().wait().unwrap();
}

