//use tokio::net::tcp::{ WriteHalf, ReadHalf };
//use tokio::io::{ BufWriter, BufReader };

mod user_state;

pub struct User<'a>
{
  state: user_state::UserState,
  //writer: tokio::net::tcp::split::TcpStreamWriteHalf,
  //reader: tokio::net::tcp::split::TcpStreamReadHalf,
  //writer: tokio::io::BufWriter<tokio::net::tcp::WriteHalf<'_>>,
  //reader: tokio::io::BufReader<tokio::net::tcp::ReadHalf<'_>>,
  //writer: tokio::io::BufWriter<tokio::net::tcp::WriteHalf<'WriteHalf>>,
  //reader: tokio::io::BufReader<tokio::net::tcp::ReadHalf<'ReadHalf>>,
  //writer: tokio::io::BufWriter,
  //reader: tokio::io::BufReader,
  //writer: tokio::net::tcp::WriteHalf<TcpStream>,
  //reader: tokio::net::tcp::ReadHalf<TcpStream>,
  //writer: &'a tokio::net::tcp::WriteHalf,
  //reader: &'a tokio::net::tcp::ReadHalf,

  pub writer: Option<&'a mut tokio::net::tcp::WriteHalf<'a>>,
  //pub writer: Option<&'a mut tokio::io::BufWriter<tokio::net::tcp::WriteHalf<'a>>>,
  //pub writer: Option<&'a mut tokio::io::BufWriter<tokio::net::tcp::WriteHalf<'a>>>,

  beeps: bool,
  hilites: bool,
  squelched: bool,
  hush_yells: bool,
  echos: bool,
  time_stamps: bool,
  system_messages: bool,
  hidden_sysop: bool,
  user_list: bool,
  changed_name: bool,
  print_time: bool,
  ssl_connected: bool,

  socket_id: i32,
  // id: u32,
  last_private_id: i16,
  channel: u16,
  level: u8,
  spam: u8,
  warnings: u8,
  login_time: u64,
  idle_time: u64,
  stamp_time: u64,
  //address: u32,
  location: String,
  web_socket_key: String,
  name: String,

  gag_list: Vec<String>,
}

/*
impl Default for User
{
  fn default() -> Self
  {
      state = user_state::UserState::Free;

      beeps: false,
      hilites: false,
      squelched: false,
      hush_yells: false,
      echos: false,
      time_stamps: false,
      system_messages: true,
      hidden_sysop: false,
      user_list: false,
      changed_name: false,
      print_time: false,
      ssl_connected: false,

      socket_id: -1,
      last_private_id: -1,
      channel: 0,
      level: 0,
      spam: 0,
      warnings: 0,
      login_time: 0,
      idle_time: 0,
      stamp_time: 0,
      location: String::from("unknown"),
      web_socket_key: String::from(""),
      name: String::from("none"),

      gag_list: Vec::new(),
  }
}
*/

impl<'a> User<'a>
{
  pub fn init() -> User<'a>
  {
    User
    {
      state: user_state::UserState::Free,
      writer: None,
      //reader: None,

      beeps: false,
      hilites: false,
      squelched: false,
      hush_yells: false,
      echos: false,
      time_stamps: false,
      system_messages: true,
      hidden_sysop: false,
      user_list: false,
      changed_name: false,
      print_time: false,
      ssl_connected: false,

      socket_id: -1,
      last_private_id: -1,
      channel: 0,
      level: 0,
      spam: 0,
      warnings: 0,
      login_time: 0,
      idle_time: 0,
      stamp_time: 0,
      location: String::from("unknown"),
      web_socket_key: String::from(""),
      name: String::from("none"),

      gag_list: Vec::new(),
    }
  }

  pub fn is_free(&self) -> bool
  {
    matches!(self.state, user_state::UserState::Free)
  }

  pub fn dump(&self)
  {
    println!("state: {}", self.state);

    println!("beeps: {}", self.beeps);
    println!("hilites: {}", self.hilites);
    println!("squelched: {}", self.squelched);
    println!("hush_yells: {}", self.hush_yells);
    println!("echos: {}", self.echos);
    println!("time_stamps: {}", self.time_stamps);
    println!("system_messages: {}", self.system_messages);
    println!("hidden_sysop: {}", self.hidden_sysop);
    println!("user_list: {}", self.user_list);
    println!("changed_name: {}", self.changed_name);
    println!("print_time: {}", self.print_time);
    println!("ssl_connected: {}", self.ssl_connected);

    println!("socket_id: {}", self.socket_id);
    println!("last_private_id: {}", self.last_private_id);
    println!("channel: {}", self.channel);
    println!("level: {}", self.level);
    println!("spam: {}", self.spam);
    println!("warnings: {}", self.warnings);
    println!("login_time: {}", self.login_time);
    println!("idle_time: {}", self.idle_time);
    println!("stamp_time: {}", self.stamp_time);
    println!("location: {}", self.location);
    println!("web_socket_key: {}", self.web_socket_key);
    println!("name: {}", self.name);

    println!("gag_list: {}", self.gag_list.len());
  }
}

